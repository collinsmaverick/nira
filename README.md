Step 1 :-
 Deploy the python rest api on the server and ensure its running by running this 
 url in the browser **https://`<ip-address or domain-name>:<port>`/CheckPRNStatus/2210000000586**
 this should return a json object

Step 2 :- Open the UraTrait.php file in the laravel project look for this method 
    public function **getPythonApiUrl()**
    {
       ** return 'http://127.0.0.1:5002/';**
    }
    Change it to the url of the python api 
    public function **getPythonApiUrl()**
    {
        **return '`https://<ip-address or domain-name>:<port>`/';**
    }
    then deploy the laravel project test it by loading this url in the browser
    **`https://<ip-address or domain-name>:<port>`/apihome/CheckPRNStatus/2210000000586**

