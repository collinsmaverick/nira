<?php

namespace App\Http\Controllers;

use App\Traits\NIRATrait;
use Illuminate\Http\Request;
use App\Traits\UraTrait;
use App\Traits\NssfApiTrait;

class HomeController extends Controller
{
    use UraTrait;
    use NssfApiTrait;
    use NIRATrait;

    public function nira()
    {
        $this->get_bearer_token();
    }
    public function index($method, $code = null)
    {

        if (isset($method, $code)) {


            if ($method == 'GetPRN') {
                $api_response = $this->GetPRN("MOLG", "DT", "LFPW", "75315", '8900', "1000027403", "name001", "MOBPMT",  "awamono@ura.go.ug", date('Y-m-d H:i:s'), 0, "City Hall", "0782896512", 0, 0, "7777777");
            } elseif ($method == 'GetTaxIdentificationNumber') {
                $api_response =  $this->GetTaxIdentificationNumber( $code);
            } elseif ($method == 'GetClientRegistration') {
                $api_response = $this->GetClientRegistration($code);
            } elseif ($method == 'CheckTaxClearanceStatus') {
                $api_response =  $this->CheckTaxClearanceStatus($code);
            }
            elseif($method == 'NIN')
            {
                $this->get_bearer_token();
                //$api_response = $this->get_NIN();
            }
            else {
                $api_response = 'At least one Parameter required';
            }

        } else {
            $api_response = 'Wrong Method Selected (apihome/{method}/{code}) e.g  (GetPRN,CheckPRNStatus,GetClientRegistration,CheckTaxClearanceStatus)';
        }

        //$api_response = $this->GetPRN("MOLG", "DT", "LFPW", "75315", '8900', "1000027403", "name001", "MOBPMT",  "awamono@ura.go.ug", date('Y-m-d H:i:s'), 0, "City Hall", "0782896512", 0, 0 );

        return response()->json($api_response, 200);
       // dd($api_response);


        $data = "fdf";

        $data = [
            'api_data' => 'hii'
        ];
        return view('apiHome', $data);
    }

}
