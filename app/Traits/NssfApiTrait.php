<?php

namespace App\Traits;

use \SoapClient;
use phpseclib\Crypt\RSA as Crypt_RSA;
use Illuminate\Support\Facades\Http;

trait NssfApiTrait
{
    public function getApiKey()
    {
        return "111";
    }
    public function getPrivateKey()
    {
        # code...        
        $file_private_key = base_path('app/mycert/privateKey.key');  
        $pkeyid = openssl_pkey_get_private(file_get_contents($file_private_key));
        return $pkeyid;
    }
     public function getEndPoint($certificate_number)
    {
        # code...
        return  "https://eservices.nssfug.org/ecertificatedv/index.php/api/validatecertificate/".$certificate_number; //in document //"https://192.168.192.124/nssfapiaggregator/public/validatecertificate/";
    }
    public function getHash($certificate_number,$private_key,$api_key)
    {
        # code... 0-|g{6E/@AW;Ug]U~@C: 
        return $certificate_number.$private_key.$api_key;

    }

    public function Caller($certificate_number)
    {
        # code...
       
        // $service_url = $this->getEndPoint($certificate_number);
        // $curl = curl_init($service_url);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        // curl_setopt($curl, CURLOPT_HTTPHEADER,  [
        //         'Signature: '.hash("sha512", $this->getHash($certificate_number,$this->getPrivateKey(),$this->getApiKey())),
        //         'APIKey: '.$this->getApiKey(),
        //         'Content-Type: application/json'
        //     ]);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
         //$curl_response = curl_exec($curl);
        
        //curl_close($curl);
        //return $curl_response;//hash("sha512", $this->getHash($certificate_number,$this->getPrivateKey(),$this->getApiKey()));// $curl_response;
        $url = "https://eservices.nssfug.org/ecertificatedv/index.php/api/validatecertificate/6231";
        $response = \Httpful\Request::get($url)
            ->expectsJson()
            ->withXTrivialHeader('APIkey :0-|g{6E/@AW;Ug]U~@C:')
            ->send();
           
            return  $response->raw_body;
    }
}