<?php

namespace App\Traits;

use \SoapClient;
use phpseclib\Crypt\RSA as Crypt_RSA;

trait UraTrait
{


    public function get_username_password()
    {
        return env('URA_USERNAME').env('URA_PASSWORD');
    }

    public function get_username()
    {
        return env('URA_USERNAME');
    }

    public function getSignedCredentials($encrypted)
    {
        return $this->sign_data($encrypted);
    }

    public function getEncryptedCredentials()
    {
        return $this->EncryptRsa($this->get_username_password());
    }
    public function getPythonApiUrl()
    {
        return 'http://localhost:5002/';
    }


    public function CheckPRNStatus($PRN)
    {

        // web service input params
         $request_param = array(
            "strPRN" => $PRN
         );

        $apiResponse = $this->Api_caller("CheckPRNStatus", $request_param);

        return $apiResponse;

    }

    public function CheckTaxClearanceStatus($certificateNumber)
    {
        // web service input params
        $request_param = array(
           "certificateNumber" => $certificateNumber        );

        $apiResponse = $this->Api_caller("CheckTaxClearanceStatus", $request_param);
        //success result or fail result

       return $apiResponse;
    }

    public function GetPRN($srcSystem, $paymentType, $taxHead, $referenceNo, $amount, $tin, $taxPayerName, $paymentMode, $Email, $assessmentDate,$AdditionalFees, $BuildingName, $ContactNo, $GrossAmount, $NoOfForms,$MobileNo)
    {
        $request_param = array(
            "amount" =>  23000,
            "buildingName" => '',
            "contactNo" =>   '0774221382',
            "county" =>     '',
            "district" =>   '',
            "email" =>      '',
            "expiryDays" => '21',
            "localCouncil" => '',
            "mobileNo" =>  '0774221382',
            "paymentBankCode" => 'EQB',
            "paymentMode" =>   'CASH' ,
            "paymentType" =>   'DT',
            "plotNo" =>      '',
            "referenceNo" =>  '41523678',
            "SRCSystem" =>   'ETAX',
            "street" =>    '',
            "subCounty" => '',
            "TIN" => '1001151892',
            "taxHead" => 'LFPW',
            "taxPayerBankCode" => '',
            "taxPayerName" => 'gender',
            "taxSubHead" => '',
            "traceCentre" =>  ''
         );

         $apiResponse = $this->Api_caller("GetPRN", $request_param);

         return $apiResponse;

    }

    public function GetClientRegistration($TIN)
    {
        $request_param = array(
            "TIN" => $TIN
         );

         $apiResponse = $this->Api_caller("GetClientRegistration", $request_param);

        return $apiResponse;
    }

    public function GetTaxIdentificationNumber($TIN)
    {
        $request_param = array(
            "TIN" => $TIN
         );

         $apiResponse = $this->Api_caller("GetTaxIdentificationNumber", $request_param);

        return $apiResponse;
    }


    public function Api_caller($method_name, $request_param)
    {
       if($method_name == "CheckPRNStatus") {
         $url = $this->getPythonApiUrl().$method_name.'/'.$request_param["strPRN"];
       }
       elseif($method_name == "GetClientRegistration")
       {
          $url = $this->getPythonApiUrl().$method_name.'/'.$request_param["TIN"];
       }
       elseif($method_name == "CheckTaxClearanceStatus")
       {
            $url = $this->getPythonApiUrl().$method_name.'/'.$request_param["certificateNumber"];
       }
       elseif($method_name == "GetTaxIdentificationNumber")
       {
            $url = $this->getPythonApiUrl().$method_name.'/'.$request_param["TIN"];
       }

       else{
            $url = $this->getPythonApiUrl().$method_name.'/';
       }
       $response = \Httpful\Request::get($url)
       ->body(json_encode($request_param))
       ->sendsJson()
       ->send();

        return  $response->raw_body;
    }




}
