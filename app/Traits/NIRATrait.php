<?php

namespace App\Traits;

use \SoapClient;
use phpseclib\Crypt\RSA as Crypt_RSA;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

trait NIRATrait
{
    private function get_bearer_token()
    {
        //todo: move credentials to .env file
        $grant_type = "client_credentials";
        $username = "nita-u@ROOT";
        $password = "yt!56H";
        $consumer_keys = "sTamEW6ft8wnBI5ngHqr1f0q4I0a";
        $consumer_secret = "sfvxyENFRfckOCAVfdt498BrWKoa";
        $endpoint = "https://api-sit.integration.go.ug:8243/token";

        $base64_String = base64_encode($consumer_keys.":".$consumer_secret);
        $headers = [
            'Authorization' => 'Basic '.$base64_String
        ];


        $content = \Http::asForm()->post($endpoint, [
            'grant_type' => $grant_type,
            'headers' => $headers
        ]);

        //get token and expiry bearer token into session
        $content = $content->json();
        dd($content);
        $bear_token = $content['access_token'];
        $token_expiry = \time() + $content['expires_in'];
        // $token_expiry = \time();

        session(['bear_token' => $bear_token, 'token_expiry' => $token_expiry]);

        return true;
    }

    public function get_NiN()
    {
        $token = session('bear_token');
        return $token;
        $client = new Client();
        $url = "https://api-sit.integration.go.ug:8243/getPerson?nationalId=CM930121003EGE";
        //$data = "{\"ProgrammeCode\":\"EEMIS\",\"IdentityNumber\":\"CM930121003EGE\"}";
        $headers = [
            'Authorization' => 'Bearer '.$token
        ];
        $response = $client->request('GET', $url, [
            'json' => \json_decode(true),
            'headers' => $headers,
            'verify'  => false,
        ]);
        $responseBody = json_decode($response->getBody());
        // $re$responseBody = "{\"ProgrammeCode\":\"EEMIS\",\"IdentityNumber\":\"CM930121003EGE\"}";
        //dd($responseBody);//
       // return response()->json($responseBody, 200);
        return $responseBody;

    }
}
