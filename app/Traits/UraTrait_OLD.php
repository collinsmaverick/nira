<?php

namespace App\Traits;

use \SoapClient;
use phpseclib\Crypt\RSA as Crypt_RSA;

trait UraTrait_OLD
{

    
    public function get_username_password()
    {
        return env('URA_USERNAME').env('URA_PASSWORD');
    }

    public function get_username()
    {
        return env('URA_USERNAME');
    }

    public function getSignedCredentials($encrypted)
    {
        return $this->sign_data($encrypted);
    }

    public function getEncryptedCredentials()
    {
        return $this->EncryptRsa($this->get_username_password());
    }

  
    public function CheckPRNStatus($PRN)
    {
       
        // web service input params
         $request_param = array(
            "strPRN" => $PRN
         );

        $apiResponse = $this->Api_caller("CheckPRNStatus", $request_param);
               
        return $apiResponse;

    }

    public function CheckTaxClearanceStatus($certificateNumber)
    {
        // web service input params
        $request_param = array(
           "certificateNumber" => $certificateNumber        );

        $apiResponse = $this->Api_caller("CheckTaxClearanceStatus", $request_param);
        //success result or fail result

       return $apiResponse;
    }

    public function GetPRN($srcSystem, $paymentType, $taxHead, $referenceNo, $amount, $tin, $taxPayerName, $paymentMode, $Email, $assessmentDate,$AdditionalFees, $BuildingName, $ContactNo, $GrossAmount, $NoOfForms)
    {
        $request_param = array(
            "SrcSystem" => $srcSystem,
            "PaymentType" => $paymentType,
            "TaxHead" => $taxHead,
            "ReferenceNo" => $referenceNo,
            "Amount" => $amount,
            "TIN" => $tin,
            "TaxHead" => $taxHead,
            "TaxpayerName" => $taxPayerName,
            "PaymentMode" => $paymentMode,
            "Email" => $Email,
            "AdditionalFees" = $AdditionalFees, //0
            "BuildingName" = $BuildingName,
            "ContactNo" = $ContactNo,
            "GrossAmount" = $GrossAmount, //0
            "NoOfForms" = $NoOfForms,//0
            "MobileNo" = $MobileNo,
            "AssessmentDate" => $assessmentDate //date('Y-m-d H:i:s')
         );

         $apiResponse = $this->Api_caller("GetPRN", $request_param);

         return $apiResponse;

    }


    // public function _GetNTRAssessment($ntrTaxHeadCode, $noForms, $grossAmount, $additionalFees)
    // {
    //     $request_param = array(
    //         "NTRTaxHeadCode" => $ntrTaxHeadCode,
    //         "NoForms" => $noForms,
    //         "GrossAmount" => $grossAmount,
    //         "AdditionalFees" => $additionalFees,
    //         "signedCredentials" => $this->sign_data($this->EncryptRsa($this->get_username_password())),
    //         "encryptedCredentials" => $this->EncryptRsa($this->get_username_password()),
    //         "userName" => $this->get_username()
    //      );

    //      $apiResponse = $this->Api_caller("GetSDTInstrumentDetail", $request_param);

    //      return $apiResponse;
    // }


    // public function GetClientRegistration($TIN)
    // {
    //     $request_param = array(
    //         "TIN" => $TIN            
    //      );

    //      $apiResponse = $this->Api_caller("GetClientRegistration", $request_param);
       
    //     return $apiResponse;
    // }

    // // public function GetAssessmentAmount($ntrTaxHeadCode, $noForms, $grossAmount, $additionalFees)
    // // {
    // //     $request_param = array(
    // //         "GetNTRAssessment" => $this->_GetNTRAssessment($ntrTaxHeadCode, $noForms, $grossAmount, $additionalFees),
    // //         "signedCredentials" => $this->sign_data($this->EncryptRsa($this->get_username_password())),
    // //         "encryptedCredentials" => $this->EncryptRsa($this->get_username_password()),
    // //         "userName" => $this->get_username()
    // //      );

    // //      $apiResponse = $this->Api_caller("GetAssessmentAmount", $request_param);

    // //      return $apiResponse;

    // // }

    // // public function GetURABankAccountDetails($bankcode)
    // // {
    // //     $request_param = array(
    // //         "BankCode" => $bankcode,
    // //         "signedCredentials" => $this->sign_data($this->EncryptRsa($this->get_username_password())),
    // //         "encryptedCredentials" => $this->EncryptRsa($this->get_username_password()),
    // //         "userName" => $this->get_username()
    // //      );

    // //      $apiResponse = $this->Api_caller("GetURABankAccountDetails", $request_param);

    // //      return $apiResponse;
    // // }
    // // public function GetSDTInstrumentDetail($instrumentId)
    // // {
    // //     $request_param = array(
    // //         "instrumentId" => $instrumentId,
    // //         "signedCredentials" => $this->sign_data($this->EncryptRsa($this->get_username_password())),
    // //         "encryptedCredentials" => $this->EncryptRsa($this->get_username_password()),
    // //         "userName" => $this->get_username()
    // //      );

    // //      $apiResponse = $this->Api_caller("GetSDTInstrumentDetail", $request_param);

    // //      return $apiResponse;
    // // }
    // // public function GetStampDutyPRN($instrumentId, $TIN)
    // // {
    // //     $request_param = array(
    // //         "instrumentId" => $instrumentId,
    // //         "TIN" => $TIN,
    // //         "signedCredentials" => $this->sign_data($this->EncryptRsa($this->get_username_password())),
    // //         "encryptedCredentials" => $this->EncryptRsa($this->get_username_password()),
    // //         "userName" => $this->get_username()
    // //      );

    // //      $apiResponse = $this->Api_caller("GetSDTInstrumentDetail", $request_param);

    // //      return $apiResponse;
    // // }
    // // public function GetPRN_Foreign_Currency($srcSystem, $paymentType, $taxHead, $referenceNo, $amount, $tin, $taxPayerName, $paymentMode, $paymentBankCode, $taxPayerBankCode,$currencyCode)
    // // {
    // //     $request_param = array(
    // //         "SrcSystem" => $srcSystem,
    // //         "PaymentType" => $paymentType,
    // //         "TaxHead" => $taxHead,
    // //         "ReferenceNo" => $referenceNo,
    // //         "Amount" => $amount,
    // //         "TIN" => $tin,
    // //         "TaxpayerName" => $taxPayerName,
    // //         "currencyCode" => $currencyCode,
    // //         "PaymentMode" => $paymentMode,
    // //         "PaymentBankCode" => $paymentBankCode,
    // //         "TaxPayerBankCode" => $taxPayerBankCode,
    // //         "signedCredentials" => $this->sign_data($this->EncryptRsa($this->get_username_password())),
    // //         "encryptedCredentials" => $this->EncryptRsa($this->get_username_password()),
    // //         "userName" => $this->get_username()

    // //      );

    // //      $apiResponse = $this->Api_caller("GetPRN_Foreign_Currency", $request_param);
    // //      //success result or fail result

    // //     // $statusCode=$apiResponse->CheckPRNStatusResult->StatusCode;

    // //     // if( $statusCode == "T" || $statusCode == "R" || $statusCode == "D"
    // //     // || $statusCode == "A" || $statusCode == "C" || $statusCode == "X")
    // //     // {
    // //     //     return $apiResponse->CheckPRNStatusResult;
    // //     // }
    // //     // else
    // //     // {
    // //     //     return $apiResponse->CheckPRNStatusResult->ErrorDesc;
    // //     // }
    // //     return $apiResponse;

    // // }


    // // public function create_byte_array($string){
    // //     $array = array();
    // //     foreach(str_split($string) as $char){
    // //         array_push($array, sprintf("%02X", ord($char)));
    // //     }
    // //     return implode(' ', $array);
    // // }

    // // public function EncryptRsa($input)
    // // {
    // //     //Correct way to encrypt for the service
    // //     $publicKeyString = base_path('app\urapub.cer');
    // //     // Load public key
    // //     if (!openssl_public_encrypt($input, $encryptedWithPublic, file_get_contents($publicKeyString))) {
    // //         echo "Error encrypting with public key\n";
    // //     }

    // //     print_r(base64_encode($encryptedWithPublic)); //base64_encode($signature)
    // //     echo "<br/><br/>";
    // //     echo "RSA";
    // //     echo "<br/><br/>";
    // //     return base64_encode($encryptedWithPublic);
        
    // // }
    
    // // public function sign_data($data_to_sign)
    // // {
    // //     /*hash the string
    // //     sign the hash with cert
    // //     return signature bytes*/
    // //      $file_private_key_path = base_path('app/mycert/priv_ura.pem');
    // //      $get_key = file_get_contents($file_private_key_path);
    // //     $rsa = new Crypt_RSA();
    // //     //$rsa->setPassword('password');
    // //     $rsa->loadKey($get_key); // private key

    // //     $rsa->setSignatureMode('CRYPT_RSA_SIGNATURE_PKCS1');
    // //     $signature = $rsa->sign($this->create_byte_array($data_to_sign));
    // //     print_r($signature);
    // //     //  $file_private_key_path = base_path('app/mycert/priv_ura.pem');
    // //     //  $get_key = file_get_contents($file_private_key_path);
    // //     //  $private_key =  openssl_get_privatekey($get_key);
    // //     //  $signSuccess = openssl_sign($data_to_sign, $signature, $private_key, OPENSSL_ALGO_SHA1);
    // //     //  $this->_verify($data_to_sign, $signature);
    // //      //print_r($input1);
    // //     // print_r(base64_encode($signature)); //base64_encode($signature)
    // //     $this->_verify($data_to_sign, base64_encode($signature));
    // //     // $yes = "Qb5sM6QwRocs+QBTCSTVz5R6utHb34z9bJ5v3Fs6Tv450liz3S8+y7slRTKqik7frXE6HAo0zo/n6jtS9JcAfclmKq7YFAnhMBN0kksUDGizC2mRMO87dNQUX+3lO2DonSckgBQJYoSMx0lmEq7veUV0NDQ7wQZ6khXNztH0BrcLkflp7vUlw2P2TqyWniCRmbeRQM+aH11rxXP6OkUy2K8kP77Elf61ThsnSVVSWOwN9ZPoo/GbWfE4UZ6LeDJELMBvey59DUshvA6jxDot7DCY1oEUfXj+lF/wvQ5jrvfxcpFAsMEoRrQV/VehqtD+VGn2MxBbugqdmNF3sLLMnz9HU0nHTyZjZtOKrXKqg2PirKRezJd4kKxYhqccLERKt7Sj1y5y5gc2byf4ox+d6DwTmdKTqsaYIuRhvkpHUJCEgyd50Ko8LeapiMCtsqH7Ay+etGzxyfsf3pSLn1ePfCSftL+gMj2UsYf8tR6pThs/Bq6PF3XZID1x48pgfUpvmcZaoh+OcHsq1IhNJI0/dzAw6/HDN7S0Qx7RR+DBNzv08mTuwEnXOBXdWwwX1W0aCJ3jUHjn1VouZNA2yJBQszlPwW5x2sMGdniTzMe9DDPcTltN9a5++oDntBJmQ6gjaoULaltb3d2PtcEtSmxrVOtAwlx62Md4GfZFSiGTxdI=";
    // //     return base64_encode($signature);//$this->create_byte_array($signature);//base64_encode($signature);// $this->create_byte_array($signature);//base64_encode($signature);//$this->create_byte_array($signature);//$byte_array;//$byte_array;//$this->create_byte_array($signature);// base64_encode($signature);

    // // }
    // // public function _verify($data, $binary_signature)
    // // {
    // //     $rsa = new Crypt_RSA();
    // //     $public_key = file_get_contents(base_path('app/mycert/openssl.crt'));
    // //     $rsa->loadKey($public_key); // public key
    // //     $yes = $rsa->verify($data, $binary_signature) ? 'verified' : 'unverified';
    // //     print_r($yes);
        
    // //     // // Check signature
    // //     // $ok = openssl_verify($data, $binary_signature, $public_key, OPENSSL_ALGO_SHA1);
        
    // //     // if ($ok == 1) {
    // //     //     print_r("signature ok (as it should be)\n");
    // //     // } elseif ($ok == 0) {
    // //     //     print_r("bad there's something wrong)\n");
    // //     // } else {
    // //     //     print_r("ugly, error checking signature\n");
    // //     // }
    // // }
 
   


    // public function Api_caller($method_name, $request_param)
    // {
    //     //   $opts = array(
    //     //          'ssl' => array(
    //     //            'verify_peer' => false,
    //     //            'verify_peer_name' => false,
    //     //            'allow_self_signed' => true
    //     //          )
    //     //        );

    //     //   $context = stream_context_create($opts);

    //     //   $wsdl   = env('URA_URL'); //"http://196.10.228.48/MDAPaymentService/PaymentServices.svc?singleWsdl";
    //     //   $client = new \SoapClient($wsdl, array('trace'=>1 , "exception" => 0));  // The trace param will show you errors stack

    //     //   try
    //     //   {
            
    //     //     //   $responce_param = $client->$method_name($request_param);
    //     //     //  // print_r($responce_param);
    //     //     //   return $responce_param;
    //     //   }
    //     //   catch (Exception $e)
    //     //   {
    //     //       //store this connection error in the db for
    //     //       echo $e->getMessage();
    //     //   }
    //     $url = 'http://127.0.0.1:5002/'.$method_name.'/'.$request_param;
    //         $response = \Httpful\Request::get($url)
    //         ->expectsJson()
    //         ->send();
           
    //         return  $response->raw_body;
    // }


  

}
